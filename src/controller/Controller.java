package controller;

import api.ISistemaRecomendacionPeliculas;
import model.data_structures.ILista;
import model.logic.SistemaRecomendacionPeliculas;
import model.vo.*;

public class Controller {

	private static ISistemaRecomendacionPeliculas sistemaDeRecomendacion = new SistemaRecomendacionPeliculas();

	public static boolean cargarPeliculasSR(String rutaPeliculas)
	{
		return sistemaDeRecomendacion.cargarPeliculasSR(rutaPeliculas);
	}
	
	public static boolean cargarRatingsSR(String rutaRatings)
	{
		return sistemaDeRecomendacion.cargarRatingsSR(rutaRatings);
	}
	
	public static boolean cargarTagsSR(String rutaTags)
	{
		return sistemaDeRecomendacion.cargarTagsSR(rutaTags);
	}
	public static int sizeMoviesSR() {
		return sistemaDeRecomendacion.sizeMoviesSR();
	}
	
	public static int sizeUsersSR() {
		return sistemaDeRecomendacion.sizeUsersSR();
	}
	
	public static int sizeTagsSR() {
		return sistemaDeRecomendacion.sizeTagsSR();
	}
	
	public static ILista<VOGeneroPelicula> peliculasPopularesSR (int n) {
		return sistemaDeRecomendacion.peliculasPopularesSR(n);
	}
	
	public static ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		return sistemaDeRecomendacion.catalogoPeliculasOrdenadoSR();
	}
	
	public static ILista<VOGeneroPelicula> recomendarGeneroSR() {
		return sistemaDeRecomendacion.recomendarGeneroSR();
	}
	
	public static ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		return sistemaDeRecomendacion.opinionRatingsGeneroSR();
	}
	
	public static ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		return sistemaDeRecomendacion.recomendarPeliculasSR(rutaRecomendacion, n);
	}
	
	public static ILista<VORating> ratingsPeliculaSR (long idPelicula) {
		return sistemaDeRecomendacion.ratingsPeliculaSR(idPelicula);
	}
	
	public static ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		return sistemaDeRecomendacion.usuariosActivosSR(n);
	}
	
	public static ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		return sistemaDeRecomendacion.catalogoUsuariosOrdenadoSR();
	}
	
	public static ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		return sistemaDeRecomendacion.recomendarTagsGeneroSR(n);
	}
	
	public static ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		return sistemaDeRecomendacion.opinionTagsGeneroSR();
	}
	
	public static ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		return sistemaDeRecomendacion.recomendarUsuariosSR(rutaRecomendacion, n);
	}
	
	public static ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		return sistemaDeRecomendacion.tagsPeliculaSR(idPelicula);
	}
	
	public static void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		sistemaDeRecomendacion.agregarOperacionSR(nomOperacion, tinicio, tfin);
	}
	
	public static ILista<VOOperacion> darHistoralOperacionesSR() {
		return sistemaDeRecomendacion.darHistoralOperacionesSR();
	}
	
	public static void limpiarHistorialOperacionesSR() {
		sistemaDeRecomendacion.limpiarHistorialOperacionesSR();
	}
	
	public static ILista<VOOperacion> darUltimasOperaciones(int n) {
		return sistemaDeRecomendacion.darUltimasOperaciones(n);
	}
	
	public static void borrarUltimasOperaciones(int n) {
		sistemaDeRecomendacion.borrarUltimasOperaciones(n);
	}
	
	public static long agregarPelicula(String titulo, int agno, String[] generos) {
		return sistemaDeRecomendacion.agregarPelicula(titulo, agno, generos);
	}
	
	public static void agregarRating(int idUsuario, int idPelicula, double rating) {
		sistemaDeRecomendacion.agregarRating(idUsuario, idPelicula, rating);
	}
	
	

}
