package model.data_structures;

public interface IPila<T> {

	/**
	 * Agrega un item al tope de la pila
	 * @param elemento a agregar
	 */
	public void push(T item);
	
	/**
	 * Elimina al elemento en el tope de la pila
	 * @return Elemento eliminado
	 */
	public T pop();
	
	/**
	 * Indica si la pila est� vac�a
	 * @return la lista est� vacia?
	 */
	public boolean isEmpty();
	
	/**
	 * N�mero de elementos en la pila
	 * @return N�mero de elementos
	 */
	public int size();
	
}
