package model.data_structures;

import java.util.Iterator;




public class ListaEncadenada<T> implements Iterable<T>, ILista<T> {

	private NodoEncadenado<T> primero;
	
	private NodoEncadenado<T> ultimo;
	
	private int posicionActual;
	
	private int tamanioLista;


	public ListaEncadenada()
	{
		primero = null;
		tamanioLista = 0;
		ultimo = null;
		posicionActual = 0;
	}
	
	public ListaEncadenada(NodoEncadenado<T> primero, NodoEncadenado<T> ultimo, int tamanioLista)
	{
		this.primero = primero;
		this.ultimo = ultimo;
		this.tamanioLista = tamanioLista;
		posicionActual = 0;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorListaEncadenada(this);		
	}


	@Override
	public void agregarElementoFinal(T elemento) {
		// TODO Auto-generated method stub
		if(primero == null)
		{
			primero = new NodoEncadenado<T>(elemento);
			ultimo = primero;
		}
		else
		{
			ultimo = ultimo.agregarElementoFinal(elemento);
		}
		tamanioLista++;
	}

	@Override
	public T darElemento(int posicion) {
		// TODO Auto-generated method stub
		return darNodoPosicion(posicion).darElemento();

	}
	
	public T darPrimerElemento()
	{
		return primero.darElemento();
	}

	public NodoEncadenado<T> darNodoPosicion(int posicion)
	{
		NodoEncadenado<T> actual = primero;
		while(actual != null && posicion > 0)
		{
			actual = actual.darSiguiente();
			posicion --;
		}
		if(posicion == 0)
		{
			return actual;
		}
		throw new IndexOutOfBoundsException("No existe el elemento en la posici�n dada.");
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return tamanioLista;
	}
	
	public int darPosicionActual()
	{
		return posicionActual;
	}
	
	public class IteradorListaEncadenada implements Iterator<T>
	{
		
		private NodoEncadenado<T> actual;
		
		public IteradorListaEncadenada(ListaEncadenada<T> listaEncadenada)
		{
			actual = listaEncadenada.primero;
		}
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual != null;
		}

		@Override
		public T next() {
			// TODO Auto-generated method stub
			NodoEncadenado<T> nodoARetornar = actual;
			actual = actual.darSiguiente();
			return nodoARetornar.darElemento();
		}
		
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return darElemento(posicionActual);
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(posicionActual >0)
		{
			posicionActual--;
			return true;
		}
		return false;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(tamanioLista-1 > posicionActual)
		{
			posicionActual++;
			return true;
		}
		return false;
	}
	public String toString()
	{
		StringBuilder respuesta = new StringBuilder("[");
		Iterator<T> iterador = iterator();
		while(iterador.hasNext())
		{
			respuesta.append(iterador.next().toString());
			if(iterador.hasNext())
			{
				respuesta.append(", ");
			}
		}
		respuesta.append("]");
		return respuesta.toString();		
	}
	
	public T eliminarPrimerElemento()
	{
		if(primero!=null)
		{
			NodoEncadenado<T> eliminar = primero;
			primero = primero.darSiguiente();
			eliminar.cambiarSiguiente(null);
			tamanioLista--;
			if(posicionActual>0) {
				posicionActual--;
			}
			return eliminar.darElemento();
		}
		else
		{
			throw new IndexOutOfBoundsException("No hay elementos en al lista");
		}
	}
	


	public void agregarAlInicio(T elem)
	{
		if (elem != null)
		{
			NodoEncadenado<T> nodoNuevo = new NodoEncadenado<T>(elem);
			
			nodoNuevo.cambiarSiguiente(primero);
			
			primero = nodoNuevo;
			
			tamanioLista ++;
		}
		else
		{
			System.out.println("Se intent� agregar un elemento nulo, por lo tanto no se agreg�");
		}
	}
	
	public void cambiarElementoEn(int pos, T elem)
	{		
		int contador = 0;
		NodoEncadenado<T> actual = primero;
		
		while (contador < pos-1)
		{
			actual = actual.darSiguiente();
			
			contador++;
		}
		
		
		actual.darSiguiente().cambiarElemento(elem);
	}
	
	public ListaEncadenada<T> partirPorLaMitad()
	{
		int mitad = tamanioLista / 2 - 1;
		NodoEncadenado<T> mitadLista = darNodoPosicion(mitad);
		ListaEncadenada<T> segundaMitad = new ListaEncadenada<>(mitadLista.darSiguiente(), ultimo, tamanioLista-mitad-1);
		tamanioLista = mitad +1;
		if(posicionActual > mitad)
		{
			posicionActual = mitad;
		}
		ultimo = mitadLista;
		ultimo.cambiarSiguiente(null);
		return segundaMitad;
	}
}
