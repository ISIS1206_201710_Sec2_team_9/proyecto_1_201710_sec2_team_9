package model.data_structures;

public class NodoDoblementeEncadenado <T>{
	
	private NodoDoblementeEncadenado<T> siguiente;
	
	private NodoDoblementeEncadenado<T> anterior;
	
	private T elemento;
	
	/**
	 * Se crea un nodo de una lista doblemente encadenada
	 * @param pAnterior elemento anterior de la lista
	 * @param pSiguiente elemento siguiente de la lista
	 * @param pElemento Objeto general
	 */
	public NodoDoblementeEncadenado(NodoDoblementeEncadenado<T> pAnterior, NodoDoblementeEncadenado<T> pSiguiente, T pElemento)
	{
		anterior = pAnterior;
		
		siguiente = pSiguiente;
		
		elemento = pElemento;
	}
	
	public void cambiarSiguiente(NodoDoblementeEncadenado<T> pSiguiente)
	{
		siguiente = pSiguiente;
	}
	
	public void cambiarAnterior(NodoDoblementeEncadenado<T> pAnterior)
	{
		anterior = pAnterior;
	}
	
	public NodoDoblementeEncadenado<T> darSiguiente ()
	{
		return siguiente;
	}
	
	public NodoDoblementeEncadenado<T> darAnterior ()
	{
		return anterior;
	}
	
	public T darElemento()
	{
		return elemento;
	}
	public void cambiarElemento(T elem)
	{
		elemento = elem;
	}
}
