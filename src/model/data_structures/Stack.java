package model.data_structures;

import java.util.Iterator;

public class Stack<T>implements IPila<T>, Iterable<T>{
	
	private int size;
	
	private ListaEncadenada<T> pila = new ListaEncadenada<T>();
	
	public Stack()
	{
		size = 0;
	}
	
	@Override
	public void push(T item) {
		// TODO Auto-generated method stub
		pila.agregarAlInicio(item);
		size++;
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		
		T popeado = pila.eliminarPrimerElemento();
		
		size--;
		return popeado;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}
	public T pick()
	{
		return pila.darPrimerElemento();
	}

	public String toString()
	{
		String respuesta = pila.toString();
		
		return respuesta;
	}

	@Override
	public Iterator<T> iterator() {
		return pila.iterator();
	}
	
}
