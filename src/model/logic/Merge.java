package model.logic;

import java.lang.reflect.Array;
import java.util.Comparator;
import java.util.Iterator;

import model.data_structures.ILista;



public class Merge
{
	private static <T extends Comparable<T>> void merge(T[] a, T[] aux, int lo, int mid, int hi)
	{

		for(int k = lo; k <= hi; k ++)
		{
			aux[k] = a[k];
		}

		int i = lo, j = mid+1;

		for(int k = lo; k <= hi; k ++)
		{
			if(i > mid) 
			{
				a[k]=aux[j++];
			}
			else if(j>hi)
			{
				a[k] = aux[i++];
			}
			else if(less(aux[j], aux[i]))
			{
				a[k] = aux[j++];
			}
			else
			{
				a[k] = aux[i++];
			}
		}
	}

	private static <T extends Comparable<T>> boolean less(T a, T aux) 
	{
		if(a.compareTo(aux) < 0)
		{
			return true;
		}
		return false;

	}

	private static <T extends Comparable<T>> void sort(T[] a, T[] aux, int lo, int hi)
	{
		if(hi<=lo) return;
		int mid = lo + (hi-lo)/2;
		sort(a,aux,lo,mid);
		sort(a, aux, mid+1, hi);
		merge(a,aux,lo,mid,hi);
	}

	public static <T extends Comparable<T>> long sort(T[] a)
	{	
		long inicioEjecucion = System.currentTimeMillis();

		if(a.length == 0){
			return 0l;
		}
		@SuppressWarnings("unchecked")
		T[] aux = (T[]) Array.newInstance(a[0].getClass(), a.length);
		sort(a, aux, 0, a.length -1);

		return System.currentTimeMillis() - inicioEjecucion;
	}
	
	public static <T> ILista<T> sort(ILista<T> listaOrdenar, Comparator<T> comparador) throws InstantiationException, IllegalAccessException
	{
		if(listaOrdenar.darNumeroElementos() <= 1)
		{
			return listaOrdenar;
		}
		ILista<T> segundaMitad = listaOrdenar.partirPorLaMitad();
		ILista<T> primeraMitadOrdenada = sort(listaOrdenar, comparador);
		ILista<T> segundaMitadOrdenada = sort(segundaMitad, comparador);
		@SuppressWarnings("unchecked")
		ILista<T> listaAuxiliar = listaOrdenar.getClass().newInstance();
		Iterator<T> iteradorPrimeraMitad = primeraMitadOrdenada.iterator();
		Iterator<T> iteradorSegundaMitad = segundaMitadOrdenada.iterator();
		T elementoActualPrimeraMitad = null;
		T elementoActualSegundaMitad = null;
		while(iteradorPrimeraMitad.hasNext()|| iteradorSegundaMitad.hasNext())
		{
			if(elementoActualPrimeraMitad == null && iteradorPrimeraMitad.hasNext())
				elementoActualPrimeraMitad = iteradorPrimeraMitad.next();
			
			if(elementoActualSegundaMitad == null && iteradorSegundaMitad.hasNext())
				elementoActualSegundaMitad = iteradorSegundaMitad.next();
			
			
			if(elementoActualSegundaMitad == null || (elementoActualPrimeraMitad !=null && comparador.compare(elementoActualPrimeraMitad, elementoActualSegundaMitad) <= 0))
			{
				listaAuxiliar.agregarElementoFinal(elementoActualPrimeraMitad);
				elementoActualPrimeraMitad = null;
			}
			else
			{
				listaAuxiliar.agregarElementoFinal(elementoActualSegundaMitad);
				elementoActualSegundaMitad = null;
			}			
		}
		
		if(elementoActualPrimeraMitad != null)
			listaAuxiliar.agregarElementoFinal(elementoActualPrimeraMitad);
		
		if(elementoActualSegundaMitad != null)
			listaAuxiliar.agregarElementoFinal(elementoActualSegundaMitad);
		
		return listaAuxiliar;

	}
}
