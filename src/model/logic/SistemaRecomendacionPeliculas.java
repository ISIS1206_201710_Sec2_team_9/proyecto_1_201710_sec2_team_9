package model.logic;

import   java.io. BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import model.data_structures.*;
import model.vo.ComparadorCatalogoOrdenado;
import model.vo.ComparadorDiferenciaRatings;
import model.vo.ComparadorPeliculasPorTags;
import model.vo.ComparadorRatingsUsuario;
import model.vo.ComparadorTagsAlfabetico;
import model.vo.ComparadorTagsTimestamp;
import model.vo.ComparadorVOPeliculaNumeroRatings;
import model.vo.ComparadorVOPeliculaPorAgno;
import model.vo.ComparadorVOPeliculaPromedioDeRatingsDescendente;
import model.vo.ComparadorVORatingTimeStamp;
import model.vo.ComparadorVOUsuarioConteo;
import model.vo.VOAgnoPelicula;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;
import api.ISistemaRecomendacionPeliculas;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {

	private static final String RUTA_PELICULAS = "data\\movies.csv";

	private static final String RUTA_RATINGS = "data\\ratings.csv"; 

	private static final String RUTA_TAGS= "data\\tags.csv"; 

	private static final String RUTA_RECOMENDACION= "data\\recomendacion.csv"; 

	private static final int PRIMERO_DISPONIBLE = 1902;

	private static final int NO_TIENE_AGNO = -1;

	private ListaEncadenada<VOAgnoPelicula> listaDePeliculas;

	private ListaEncadenada<String> listaDeGeneros;

	private ListaEncadenada<VOUsuario> listaDeUsuarios = new ListaEncadenada<VOUsuario>();

	private ListaEncadenada<VOTag> listaDeTags = new ListaEncadenada<VOTag>();

	private ListaEncadenada<VORating> listaDeRatings = new ListaEncadenada<VORating>();

	private ListaEncadenada<VOGeneroPelicula> peliculasPorGenero = new ListaEncadenada<VOGeneroPelicula>();

	private ListaEncadenada<VOGeneroUsuario> usuariosPorGenero = new ListaEncadenada<VOGeneroUsuario>();

	private ListaEncadenada<VOGeneroTag> tagsPorGenero = new ListaEncadenada<VOGeneroTag>();

	private int cantidadDeTags;

	private int cantidadDePeliculas;

	private Stack<VOOperacion> colaOperaciones = new Stack<>();

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {

		boolean sePudoCargar = false;
		ListaEncadenada<VOPelicula> listaPeliculas = new ListaEncadenada<VOPelicula>();
		ListaEncadenada<String> listaGeneros = new ListaEncadenada<String>();
		long tiempoDeInicio = System.currentTimeMillis();
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(RUTA_PELICULAS));
			br.readLine();
			String linea = br.readLine();

			while (linea != null)//Voy a examinar cada linea
			{					
				String[] datos = linea.split(",");

				//Id de pel�cula

				long idPelicula = Long.parseLong(datos[0]);

				//G�neros
				ListaEncadenada<String> generos = new ListaEncadenada<String>();
				int agnoPelicula = 0;
				String titulo = "";

				//generosRaw guarda el arreglo de los g�neros partidos
				String[] generosRaw;
				//El �ltimo elemento despues de partir por comas, siempre ser�n los g�neros
				generosRaw = datos[datos.length -1].split("\\|");

				//Por cada genero que haya en ese arreglo, lo agrego a la lista de g�neros
				for (int i = 0; i < generosRaw.length; i++)
				{
					generos.agregarElementoFinal(generosRaw[i]);
					Iterator<String> iter = listaGeneros.iterator();
					boolean estaEnGeneros = false;

					while (iter.hasNext() && !estaEnGeneros)
						if(iter.next().equals(generosRaw[i]))
							estaEnGeneros = true;

					if (!estaEnGeneros) //si no est� en la lista de g�neros, lo tengo que agregar
						listaGeneros.agregarElementoFinal(generosRaw[i]);
					//System.out.println(generosRaw[i]);

				}

				//Ahora miramos el a�o y el nombre
				//Caso 1: caso especial a�o y nombre, cuando tienen comas en el nombre
				if (datos.length > 3)
				{
					String nombreBueno = "";				

					//Cogo todo los elementos de la linea partida por comas, menos el primero y el �ltimo
					for (int i = 1; i < datos.length - 1; i++)
					{
						if (i != 1)
							nombreBueno = nombreBueno + "," + datos[i];
						else
							nombreBueno = nombreBueno + datos[i];
					}

					//le quito todas las comillas
					nombreBueno = nombreBueno.replaceAll("\"", "");

					//Para buscar el a�o, parto el string en los par�ntesis
					String[] agnoArray = nombreBueno.split("\\(");

					//el a�o siempre ser� el �ltimo de dicho arreglo, tambi�n le debo quitar el otro par�ntesis
					String agnoString = agnoArray[ agnoArray.length-1 ].replaceAll("\\)", "");

					//algunas veces tienen gu�ones en el a�o, para esto solo hago split en el gui�n y cogo la primer casilla
					agnoString = agnoString.split("-")[0];

					try 
					{
						agnoPelicula = Integer.parseInt(agnoString);
					}

					//Si incluso despues de todo eso, lo que resulta no es un n�mero, signfica
					//Que lo que hay ah� no es un n�mero
					catch (NumberFormatException e)
					{
						//System.out.println("No tiene a�o: " + agnoString);
						agnoPelicula = -1;
					}



					if (nombreBueno.contains(" (19") || nombreBueno.contains(" (20"))
						titulo = nombreBueno.substring(0, nombreBueno.length() - 7);				
					else
						titulo = nombreBueno;

					//System.out.println(titulo+ "\n" + agnoPelicula  + "\n" + generos);
				}
				else //es normal
				{
					String datoTitulo = datos[1].trim();
					if(datoTitulo.startsWith("\""))
					{
						agnoPelicula = Integer.parseInt(datoTitulo.substring(datoTitulo.length() -6, datoTitulo.length() -2));
					}
					else
					{
						String substringSupuestoA�o = datoTitulo.substring(datoTitulo.length() -5, datoTitulo.length() -1);
						try
						{
							agnoPelicula = Integer.parseInt(substringSupuestoA�o);
							titulo = datoTitulo.substring(0, datoTitulo.length() -6);
						}
						catch(NumberFormatException e)
						{
							agnoPelicula = -1;
							titulo = datos[1];
						}
					}

					//System.out.println(titulo+"\n"+agnoPelicula+"\n"+generos);
				}

				//Con la linea completamente parseada, croe el VO pel�cula

				VOPelicula voPel = new VOPelicula();

				voPel.setNumeroRatings(0);//Default
				voPel.setPromedioRatings(0);

				voPel.setIdUsuario(idPelicula);
				voPel.setAgnoPublicacion(agnoPelicula);
				voPel.setGenerosAsociados(generos);
				voPel.setTitulo(titulo);

				listaPeliculas.agregarElementoFinal(voPel);
				cantidadDePeliculas++;

				linea = br.readLine();
			}

			br.close();			
		}
		catch (IOException e)
		{
			System.out.println("File not found");
			e.printStackTrace();
		}

		//ahora que ya tengo todas las pel�culas en una lista, hago la de los a�os

		ListaEncadenada<VOAgnoPelicula> listaAgnos = new ListaEncadenada<VOAgnoPelicula>(null, null, 0);

		//A�adir� primero los que no tienen a�o, puesto que pueden alterar el orden

		VOAgnoPelicula agnoNoExistente = new VOAgnoPelicula();

		agnoNoExistente.setAgno(NO_TIENE_AGNO);

		Iterator<VOPelicula> iter = listaPeliculas.iterator();

		ListaEncadenada<VOPelicula> temp = new ListaEncadenada<VOPelicula>();

		while (iter.hasNext())
		{
			VOPelicula peliculaActual = iter.next();

			if (peliculaActual.getAgnoPublicacion() == NO_TIENE_AGNO)
				temp.agregarElementoFinal(peliculaActual);				
		}

		agnoNoExistente.setPeliculas(temp);

		listaAgnos.agregarElementoFinal(agnoNoExistente);

		//Este recorrido inicia desde el primer a�o de los valores hasta el a�o actual

		for (int i = PRIMERO_DISPONIBLE; i <= 2017; i++ )
		{
			VOAgnoPelicula agnoActual = new VOAgnoPelicula();

			int numAgnoActual = i;
			agnoActual.setAgno(numAgnoActual);

			iter = listaPeliculas.iterator();

			temp = new ListaEncadenada<VOPelicula>();

			while (iter.hasNext())
			{
				VOPelicula peliculaActual = iter.next();
				peliculaActual.setTagsAsociados(new ListaEncadenada<String>());

				if (peliculaActual.getAgnoPublicacion() == numAgnoActual)
					temp.agregarElementoFinal(peliculaActual);				
			}

			agnoActual.setPeliculas(temp);

			listaAgnos.agregarElementoFinal(agnoActual);
		}

		listaDePeliculas = listaAgnos;
		//System.out.println(listaDePeliculas.darNumeroElementos());

		//ahora la de los g�neros

		listaDeGeneros = listaGeneros;

		if (listaDePeliculas != null)
			sePudoCargar = true;
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		System.out.println(tiempoDeEjecucion);
		return sePudoCargar;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		// TODO Auto-generated method stub

		long tiempoDeInicio = System.currentTimeMillis();
		boolean sePudoCargar = false;
		BufferedReader br;

		try
		{
			br = new BufferedReader(new FileReader(new File(RUTA_RATINGS)));

			//Hago esta lectura para saltar la primera l�nea
			br.readLine();

			String linea = br.readLine();
			VOUsuario usuarioCiclo = null;

			while (linea != null)
			{
				String[] datos = linea.split(",");
				//System.out.println( "Voy en: " + Arrays.toString(datos));

				long idUsuario = Long.parseLong(datos[0]); //Us
				long idPelicula = Long.parseLong(datos[1]); //Peli 
				double rating = Double.parseDouble(datos[2]); //Peli
				long timeStamp = Long.parseLong(datos[3]); //Us

				VORating voRating = new VORating();

				voRating.setIdPelicula(idPelicula);
				voRating.setIdUsuario(idUsuario);
				voRating.setRating(rating);

				listaDeRatings.agregarElementoFinal(voRating);

				Iterator<VOUsuario> iterUsuarios = listaDeUsuarios.iterator();

				if (listaDeUsuarios.darNumeroElementos() != 0)
				{
					boolean encontreUsuario = false;

					while (iterUsuarios.hasNext() && !encontreUsuario)
					{

						VOUsuario actual = iterUsuarios.next();
						if (actual.getIdUsuario() == idUsuario)
						{
							encontreUsuario = true;
							usuarioCiclo = actual;

							actual.setNumRatings( 1 + actual.getNumRatings());
							if (actual.getPrimerTimestamp() > timeStamp)
							{
								actual.setPrimerTimestamp(timeStamp);
							}
						}							
					}

					if (!encontreUsuario)
					{
						VOUsuario nuevo = new VOUsuario();
						nuevo.setIdUsuario(idUsuario);
						nuevo.setNumRatings(1);
						nuevo.setPrimerTimestamp(timeStamp);
						nuevo.setTags(new ListaEncadenada<VOTag>());						

						usuarioCiclo = nuevo;
						listaDeUsuarios.agregarElementoFinal(nuevo);
					}
				}
				else
				{
					VOUsuario nuevo = new VOUsuario();
					nuevo.setIdUsuario(idUsuario);
					nuevo.setNumRatings(1);
					nuevo.setPrimerTimestamp(timeStamp);
					nuevo.setTags(new ListaEncadenada<VOTag>());

					listaDeUsuarios.agregarElementoFinal(nuevo);
				}

				//Agrego usuarios al g�nero en que est�n

				VOPelicula peli = buscarPeliculaPorId(idPelicula);

				Iterator<String> iterGenerosPeli = peli.getGenerosAsociados().iterator();

				while (iterGenerosPeli.hasNext())
				{					
					String generoPeliActual = iterGenerosPeli.next();

					if (usuariosPorGenero.darNumeroElementos() == 0) //Si los usuarios por g�nero est�n vac�os, debo agregar algo a la lista
					{
						VOUsuarioConteo nuevo = new VOUsuarioConteo();
						nuevo.setConteo(1);
						nuevo.setIdUsuario(idUsuario);

						ListaEncadenada<VOUsuarioConteo> nuevaLista = new ListaEncadenada<VOUsuarioConteo>();
						nuevaLista.agregarElementoFinal(nuevo);

						VOGeneroUsuario nuevoGenero = new VOGeneroUsuario();
						nuevoGenero.setGenero(generoPeliActual);
						nuevoGenero.setUsuarios(nuevaLista);

						usuariosPorGenero.agregarElementoFinal(nuevoGenero);
					}
					else
					{
						Iterator<VOGeneroUsuario> iterGeneros = usuariosPorGenero.iterator();
						boolean encontreGenero = false;
						while (iterGeneros.hasNext())
						{
							VOGeneroUsuario VOActual = iterGeneros.next();
							String generoActualLocal = VOActual.getGenero();

							if (generoActualLocal.equals(generoPeliActual))
							{
								encontreGenero = true;
								//si elgenero de esta pel�cula es igual a la iteraci�n de usuario actual, debo agregar
								//dicho usuario a esa lista de usuarios por g�nero
								Iterator<VOUsuarioConteo> iterUsuariosLocal = VOActual.getUsuarios().iterator();
								boolean estaEnListaUsuariosGenero = false;

								while (iterUsuariosLocal.hasNext() && !estaEnListaUsuariosGenero)
								{
									VOUsuarioConteo usuarioLocal = iterUsuariosLocal.next();

									//Si ya est� en la lista, no lo debo agregar y solamente le sumo el conteo
									if (usuarioCiclo.getIdUsuario() == usuarioLocal.getIdUsuario())
									{
										estaEnListaUsuariosGenero = true;
										usuarioLocal.setConteo( usuarioLocal.getConteo() + 1);
									}
								}

								if (!estaEnListaUsuariosGenero)
								{
									VOUsuarioConteo nuevo = new VOUsuarioConteo();
									nuevo.setConteo(1);
									nuevo.setIdUsuario(idUsuario);

									VOActual.getUsuarios().agregarElementoFinal(nuevo);
								}
							}
						}

						if (!encontreGenero) //Si no esncuentro un g�nero, entonces debo agregarlo con el usuario actual
						{
							VOUsuarioConteo nuevo = new VOUsuarioConteo();
							nuevo.setConteo(1);
							nuevo.setIdUsuario(idUsuario);

							ListaEncadenada<VOUsuarioConteo> nuevaLista = new ListaEncadenada<VOUsuarioConteo>();
							nuevaLista.agregarElementoFinal(nuevo);

							VOGeneroUsuario nuevoGenero = new VOGeneroUsuario();
							nuevoGenero.setGenero(generoPeliActual);
							nuevoGenero.setUsuarios(nuevaLista);

							usuariosPorGenero.agregarElementoFinal(nuevoGenero);
						}
					}
				}


				//				VORating ratingActual = new VORating();
				//				ratingActual.setIdPelicula(idPelicula);
				//				ratingActual.setIdUsuario(idUsuario);
				//				ratingActual.setRating(rating);

				Iterator<VOAgnoPelicula> iterAgno = listaDePeliculas.iterator();

				//Agrego el rating a la ple�cula

				while (iterAgno.hasNext())
				{
					VOAgnoPelicula agnoActual = iterAgno.next();

					Iterator<VOPelicula> iterPelicula = agnoActual.getPeliculas().iterator();

					while (iterPelicula.hasNext())
					{
						VOPelicula peliculaActual = iterPelicula.next();

						if (peliculaActual.getIdPelicula() == idPelicula)
						{
							peliculaActual.setPromedioRatings(nuevoPromedio(peliculaActual, rating));
							peliculaActual.setNumeroRatings( peliculaActual.getNumeroRatings() + 1 );
							//System.out.println(peliculaActual);
						}
					}
				}
				linea = br.readLine();
			}

			//System.out.println("Acab�");
			br.close();
			sePudoCargar = true;

		}catch (FileNotFoundException e)
		{
			System.out.println("Error al cargar el archivo de ratings");
		}catch (IOException e)
		{
			System.out.println("Error al leer el archivo: " + e);
			e.printStackTrace();
		}

		//System.out.println("CARGAR RATINGS:\n"+listaDeUsuarios + "\n\n -----USUARIOS/GENERO----- \n\n" + usuariosPorGenero +  "\n\n -----FIN CARGAR RATINGS----- \n\n");

		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		System.out.println(tiempoDeEjecucion);
		return sePudoCargar;
	}

	private double nuevoPromedio (VOPelicula pelicula, double calificacion)
	{
		double nuevoPromedio = 0;

		nuevoPromedio = (pelicula.getPromedioRatings() * pelicula.getNumeroRatings() + calificacion)
				/(pelicula.getNumeroRatings()+1);		

		return nuevoPromedio;
	}

	private VOPelicula buscarPeliculaPorId(long idPelicula) {

		Iterator<VOAgnoPelicula> iter = listaDePeliculas.iterator();

		while (iter.hasNext())
		{
			VOAgnoPelicula agnoActual = iter.next();
			Iterator<VOPelicula> iterPelis = agnoActual.getPeliculas().iterator();

			while (iterPelis.hasNext())
			{
				VOPelicula actual = iterPelis.next();

				if (actual.getIdPelicula() == idPelicula)
				{
					return actual;
				}
			}
		}

		System.out.println("null :(");

		return null;
	}

	private void inicializarPeliculasPorGenero()
	{
		Iterator<String> iterGeneros = listaDeGeneros.iterator();		

		while (iterGeneros.hasNext())
		{
			ListaEncadenada<VOPelicula> listaGeneroActual = new ListaEncadenada<VOPelicula>();
			String generoActual = iterGeneros.next();

			Iterator<VOAgnoPelicula> iterAgnos = listaDePeliculas.iterator();

			while (iterAgnos.hasNext())
			{
				Iterator<VOPelicula> iterPelis = iterAgnos.next().getPeliculas().iterator();

				while (iterPelis.hasNext())
				{	
					VOPelicula actual = iterPelis.next();

					Iterator<String> iterGenerosPeli = actual.getGenerosAsociados().iterator();

					boolean encontreGenero = false;

					while (iterGenerosPeli.hasNext() && !encontreGenero)
					{
						if (iterGenerosPeli.next().equals(generoActual))
						{
							listaGeneroActual.agregarElementoFinal(actual);
							encontreGenero = true;
						}						
					}
				}				
			}

			VOGeneroPelicula nuevoGeneroPeli = new VOGeneroPelicula();

			nuevoGeneroPeli.setGenero(generoActual);
			nuevoGeneroPeli.setPeliculas(listaGeneroActual);

			//Debugging / visualizaci�n de qu� est� pasando
			//System.out.println("------\nG�nero: " + generoActual + "\n\n" + listaGeneroActual);

			peliculasPorGenero.agregarElementoFinal(nuevoGeneroPeli); 
		}

	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		// TODO Auto-generated method stub

		long tiempoDeInicio = System.currentTimeMillis();
		boolean sePudoCargar = false;

		BufferedReader br;

		try 
		{
			br = new BufferedReader( new FileReader (new File(RUTA_TAGS)));
			br.readLine();

			String linea = br.readLine();

			while (linea != null)
			{
				String[] datos = linea.split(",");

				long idUsuario;
				long idPelicula;
				String tag;
				long timeStamp;				

				try
				{

					idUsuario = Long.parseLong(datos[0]);
					idPelicula = Long.parseLong(datos[1]);
					tag = datos[2];
					timeStamp = Long.parseLong(datos[3]);
				}
				catch (NumberFormatException e)//si algo sale mal, hay mas de 1 coma
				{
					idUsuario = Long.parseLong(datos[0]);
					idPelicula = Long.parseLong(datos[1]);
					timeStamp = Long.parseLong(datos[datos.length-1]);

					datos[0] = "";
					datos[1] = "";
					datos[datos.length - 1] = "";

					tag = String.join(",", datos).replaceAll(",,\"", "").replaceAll("\",", "");

					//Tag extenso, con comas
					//System.out.println(tag);

				}

				VOTag tagActual = new VOTag();

				tagActual.setTag(tag);
				tagActual.setTimestamp(timeStamp);

				listaDeTags.agregarElementoFinal(tagActual);

				VOPelicula peliActual = buscarPeliculaPorId(idPelicula);

				if (peliActual.getTagsAsociados() == null)
				{
					peliActual.setTagsAsociados( new ListaEncadenada<String>());
					peliActual.setNumeroTags(0);
				}


				Iterator<String> iterTagsActual = peliActual.getTagsAsociados().iterator();
				boolean tieneTag = false;

				while (iterTagsActual.hasNext() && !tieneTag)
				{
					if (iterTagsActual.next().equals(tag))								
						tieneTag = true;													
				}
				if (!tieneTag) //si no tiene el tag, se lo agrego
				{
					peliActual.getTagsAsociados().agregarElementoFinal(tag);
					peliActual.setNumeroTags( peliActual.getNumeroTags() + 1);

					cantidadDeTags++;
					//System.out.println("agregu� a "+ peliActual.getTitulo() + ": " + tag);
				}
				//Inicializar lista de usuarios
				//si la lista de usuarios est� vac�a, agrego uno nuevo con los datos de la l�nea
				if (listaDeUsuarios.darNumeroElementos() == 0)
				{
					VOUsuario nuevo = new VOUsuario();
					nuevo.setIdUsuario(idUsuario);
					nuevo.setNumRatings(0);
					nuevo.setPrimerTimestamp(timeStamp);
					ILista<VOTag> nuevosTag = new ListaEncadenada<VOTag>();
					nuevosTag.agregarElementoFinal(tagActual);
					nuevo.setTags(nuevosTag);

					listaDeUsuarios.agregarElementoFinal(nuevo);
				}
				else
				{
					Iterator<VOUsuario> iterUsuarios = listaDeUsuarios.iterator();

					boolean encontre = false;

					while (iterUsuarios.hasNext() && !encontre)
					{
						VOUsuario actual = iterUsuarios.next();

						encontre = actual.getIdUsuario() == idUsuario;

						if (encontre)
						{
							actual.getTags().agregarElementoFinal(tagActual);

							//System.out.println("le agregu� " + tag + " a " + idUsuario);

							if (actual.getPrimerTimestamp() > timeStamp)
								actual.setPrimerTimestamp(timeStamp);							
						}

					}

					if (!encontre) //si no encontr�, tengo que agregar al men
					{
						VOUsuario nuevo = new VOUsuario();
						nuevo.setIdUsuario(idUsuario);
						nuevo.setNumRatings(1);
						nuevo.setPrimerTimestamp(timeStamp);

						ILista<VOTag> nuevosTag = new ListaEncadenada<VOTag>();
						nuevosTag.agregarElementoFinal(tagActual);

						//System.out.println("le agregu� " + tag + "a " + idUsuario);

						nuevo.setTags(nuevosTag);						

						listaDeUsuarios.agregarElementoFinal(nuevo);
					}

					//System.out.println("he agregado " + cantidadDeTags + " tags");
				}
				linea = br.readLine();
			}

			Iterator<VOAgnoPelicula> iterAgnos = listaDePeliculas.iterator();

			while (iterAgnos.hasNext())
			{
				Iterator<VOPelicula> iterPeliculas = iterAgnos.next().getPeliculas().iterator();

				while (iterPeliculas.hasNext())
				{
					VOPelicula peliActual = iterPeliculas.next();

					//Ahora los tagsPorGenero
					Iterator<String> iterGenerosActual = peliActual.getGenerosAsociados().iterator();

					while (iterGenerosActual.hasNext())
					{
						String generoActual = iterGenerosActual.next();
						//Si est� vacio el atributo, lo inicializo
						if (tagsPorGenero.darNumeroElementos() == 0)
						{
							VOGeneroTag nuevoGT = new VOGeneroTag();
							ListaEncadenada<String> nuevosTags = new ListaEncadenada<String>();

							nuevoGT.setGenero(generoActual);
							nuevoGT.setTags(nuevosTags);
							tagsPorGenero.agregarElementoFinal(nuevoGT);
						}
						//si no, busco por todos los g�neros tag y el que me coincida con el g�nero de la pel�cula actual
						//Agrego auqellos tags que no est�n repetidos
						else
						{
							Iterator<VOGeneroTag> iterGeneroTag = tagsPorGenero.iterator();

							boolean encontreGenero = false;

							while (iterGeneroTag.hasNext() && !encontreGenero)
							{
								VOGeneroTag GTActual = iterGeneroTag.next();

								//System.out.println(GTActual.getGenero() + " vs " + generoActual);
								if (GTActual.getGenero().equals(generoActual))
								{
									encontreGenero = true;

									Iterator<String> iterTagsPeli = peliActual.getTagsAsociados().iterator();

									boolean estaRepetido = false;

									while (iterTagsPeli.hasNext())
									{
										String tagPeliActual = iterTagsPeli.next();
										Iterator<String> iterTagsGT = GTActual.getTags().iterator();

										while(iterTagsGT.hasNext() && !estaRepetido)
										{
											String tagGTActual = iterTagsGT.next();
											if (tagGTActual.equals(tagPeliActual))
											{ //si son iguales, hay un repetido
												estaRepetido = true;
											}
										}
										if (!estaRepetido)
										{
											GTActual.getTags().agregarElementoFinal(tagPeliActual);
											//System.out.println("agregu�: " + tagPeliActual);
										}
									}
								}										
							}

							if (!encontreGenero)
							{
								VOGeneroTag nuevoGT = new VOGeneroTag();
								ListaEncadenada<String> nuevosTags = new ListaEncadenada<String>();

								nuevoGT.setTags(nuevosTags);										
								nuevoGT.setGenero(generoActual);

								tagsPorGenero.agregarElementoFinal(nuevoGT);
							}
						}
					}


				}
			}

			br.close();
			inicializarPeliculasPorGenero();
			//System.out.println(tagsPorGenero); para ver los tags por c�da g�nero (est� bien)
			sePudoCargar = true;

		}catch (FileNotFoundException e)
		{
			System.out.println("Error al cargar el archivo de ratings");
		}catch (IOException e)
		{
			System.out.println("Error al leer el archivo: " + e);
			e.printStackTrace();
		}	
		//System.out.println("CARGAR TAGS:\n"+listaDeUsuarios+ "\n\n -----FIN CARGAR TAGS----- \n\n");

		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		System.out.println(tiempoDeEjecucion);
		return sePudoCargar;
	}
	@Override
	public int sizeMoviesSR() 
	{
		return cantidadDePeliculas;
	}

	@Override
	public int sizeUsersSR() 
	{
		return listaDeUsuarios.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() 
	{
		return cantidadDeTags;
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		long tiempoDeInicio = System.currentTimeMillis();
		ListaEncadenada <VOGeneroPelicula> listaRetornar = new ListaEncadenada<VOGeneroPelicula>();

		Iterator<VOGeneroPelicula> iterGenero = peliculasPorGenero.iterator();
		while(iterGenero.hasNext())
		{
			VOGeneroPelicula voGeneroActual = iterGenero.next();
			String genero = voGeneroActual.getGenero();

			try {
				ListaEncadenada<VOPelicula> peliculasGenero = (ListaEncadenada<VOPelicula>) Merge.sort(voGeneroActual.getPeliculas(),new ComparadorVOPeliculaNumeroRatings());			
				Iterator<VOPelicula> iterPeliculas = peliculasGenero.iterator();
				int contadorPosicion = 0;
				ListaEncadenada<VOPelicula> peliculasAgregar = new ListaEncadenada<>();
				if(n < peliculasGenero.darNumeroElementos())
				{
					while(iterPeliculas.hasNext() && contadorPosicion < n)
					{
						VOPelicula peliculaActual = iterPeliculas.next();
						peliculasAgregar.agregarElementoFinal(peliculaActual);
						contadorPosicion ++;
					}
				}
				else
				{
					while(iterPeliculas.hasNext())
					{
						VOPelicula peliculaActual = iterPeliculas.next();
						peliculasAgregar.agregarElementoFinal(peliculaActual);
					}
				}
				voGeneroActual.setPeliculas(peliculasAgregar);
				voGeneroActual.setGenero(genero);
				listaRetornar.agregarElementoFinal(voGeneroActual);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("peliculasPopularesSR", tiempoDeInicio, tiempoDeEjecucion);
		return listaRetornar;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		long tiempoDeInicio = System.currentTimeMillis();
		ListaEncadenada<VOPelicula> listaRetornar = new ListaEncadenada<>();
		Iterator<VOAgnoPelicula> iterPeliPorAgno = listaDePeliculas.iterator();
		while(iterPeliPorAgno.hasNext())
		{
			VOAgnoPelicula voAgnoPeliculaActual = iterPeliPorAgno.next();
			//			System.out.println(voAgnoPeliculaActual.getAgno());
			ListaEncadenada<VOPelicula> listaPeliculasAgno;

			listaPeliculasAgno = (ListaEncadenada<VOPelicula>) voAgnoPeliculaActual.getPeliculas();
			//System.out.println(listaPeliculasAgno);
			Iterator<VOPelicula> iterPeliculas = listaPeliculasAgno.iterator();
			while(iterPeliculas.hasNext())
			{
				VOPelicula peliculaActual = iterPeliculas.next();
				listaRetornar.agregarElementoFinal(peliculaActual);
			}				
		}
		try {
			listaRetornar = (ListaEncadenada<VOPelicula>) Merge.sort(listaRetornar, new ComparadorCatalogoOrdenado());
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("catalogoPeliculasOrdenadoSR", tiempoDeInicio, tiempoDeEjecucion);
		return listaRetornar;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		long tiempoDeInicio = System.currentTimeMillis();
		ListaEncadenada<VOGeneroPelicula> listaRetornar = new ListaEncadenada<>();
		Iterator<VOGeneroPelicula> iterGeneros = peliculasPorGenero.iterator();
		while(iterGeneros.hasNext())
		{
			try {
				VOGeneroPelicula generoActual = iterGeneros.next();
				ListaEncadenada<VOPelicula> listaPeliculasGeneroActual = (ListaEncadenada<VOPelicula>) generoActual.getPeliculas();
				ListaEncadenada<VOPelicula> listaPeliculasRetornar = new ListaEncadenada<>();

				listaPeliculasGeneroActual = (ListaEncadenada<VOPelicula>) Merge.sort(listaPeliculasGeneroActual, new ComparadorVOPeliculaPromedioDeRatingsDescendente());
				Iterator<VOPelicula> iterPeliculasGenero = listaPeliculasGeneroActual.iterator();
				//Se agrega numeroPeliculas por genero para retornar
				int numeroPeliculas = 1;
				int contador = 0;
				while(iterPeliculasGenero.hasNext() && contador < numeroPeliculas)
				{
					VOPelicula peliculaActual = iterPeliculasGenero.next();
					listaPeliculasRetornar.agregarElementoFinal(peliculaActual);
					contador ++;
				}
				generoActual.setPeliculas(listaPeliculasRetornar);
				listaRetornar.agregarElementoFinal(generoActual);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("recomendarGeneroSR", tiempoDeInicio, tiempoDeEjecucion);
		System.out.println(listaRetornar);
		return listaRetornar;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		long tiempoDeInicio = System.currentTimeMillis();
		ListaEncadenada<VOGeneroPelicula> listaRetornar = new ListaEncadenada<>();
		Iterator<VOGeneroPelicula> iterGenero = peliculasPorGenero.iterator();
		while(iterGenero.hasNext())
		{
			VOGeneroPelicula voGeneroActual = iterGenero.next();
			ListaEncadenada<VOPelicula> peliculasGeneroOrdenadas = new ListaEncadenada<>();
			try {
				peliculasGeneroOrdenadas = (ListaEncadenada<VOPelicula>) Merge.sort(voGeneroActual.getPeliculas(), new ComparadorVOPeliculaPorAgno());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			voGeneroActual.setPeliculas(peliculasGeneroOrdenadas);
			listaRetornar.agregarElementoFinal(voGeneroActual);

		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("opinionRatingsGeneroSR", tiempoDeInicio, tiempoDeEjecucion);
		System.out.println(listaRetornar);
		return listaRetornar;	
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) 
	{
		long tiempoDeInicio  = System.currentTimeMillis();
		ListaEncadenada<VOPeliculaPelicula> listaRetornar = new ListaEncadenada<>();
		System.out.println("Empezo");
		BufferedReader br;
		Queue<String[]> colaInformacion = new Queue<>();

		//Se guarda toda la informaci�n del csv en una cola.
		try {
			br = new BufferedReader(new FileReader(RUTA_RECOMENDACION));
			//Hago esta lectura para saltar la primera l�nea
			br.readLine();

			String linea = br.readLine();
			int contador = 0;
			while(linea != null && contador < n)
			{
				System.out.println(linea);
				String[] infoPelicula = linea.split(",");
				colaInformacion.enqueue(infoPelicula);
				linea = br.readLine();
				contador ++;
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int contadorRecorrido = 0;
		//Se empieza a procesar la informaci�n
		while(contadorRecorrido < colaInformacion.size())
		{
			String[] infoCola = colaInformacion.dequeue();
			// idPelicula de la cola
			long idPelicula = Long.parseLong(infoCola[0]);
			//rating asociado pelicula cola
			double rating = Double.parseDouble(infoCola[1]);
			//Lista con la pelicula y las peliculas relacionadas para agregar a la lista a retornar
			VOPeliculaPelicula agregarListaRetornar = new VOPeliculaPelicula();
			//VOPelicula de la pelicula con el idPelicula
			VOPelicula peliculaActual = buscarPelicula(idPelicula);
			//Se agrega la pelicula actual
			agregarListaRetornar.setPelicula(peliculaActual);
			//Se saca el genero principal de la pelicula a analizar
			String generoPrincipalPeliculaActual = peliculaActual.getGenerosAsociados().darElemento(0);
			Iterator<VOGeneroPelicula> iterGeneros = peliculasPorGenero.iterator();
			int contador = 0;
			//Se recorre por generos para encontrar la lista de peliculas del genero principal de la pelicula
			while(iterGeneros.hasNext() && contador < n)
			{
				System.out.println("genero" + contador);
				VOGeneroPelicula voGeneroActual = iterGeneros.next();
				if(voGeneroActual.getGenero().equals(generoPrincipalPeliculaActual) )
				{
					//Lista peliculas mismo genero
					ListaEncadenada<VOPelicula> peliculasGenero = (ListaEncadenada<VOPelicula>) voGeneroActual.getPeliculas();
					//Lista con las peliculas asociadas
					ListaEncadenada<VOPelicula> peliculasAsociadas = new ListaEncadenada<>();
					Iterator<VOPelicula> iterPeliculas = peliculasGenero.iterator();
					int contador2 = 0;
					while(iterPeliculas.hasNext())
					{
						System.out.println("pelicula"+contador2);
						VOPelicula peliculaActualGenero = iterPeliculas.next();
						double diferenciaRatings = Math.abs((rating - peliculaActualGenero.getPromedioRatings()));
						if(diferenciaRatings < 0.5)
						{
							//Se agrega la pelicula asociada
							peliculasAsociadas.agregarElementoFinal(peliculaActualGenero);
						}
						contador2 ++;
					}
					agregarListaRetornar.setPeliculasRelacionadas(peliculasAsociadas);
				}
				contador ++;
			}
			listaRetornar.agregarElementoFinal(agregarListaRetornar);
			contadorRecorrido ++;
		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("recomendarPeliculasSR", tiempoDeInicio, tiempoDeEjecucion);

		System.out.println(listaRetornar + "\nFINAL");
		return listaRetornar;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		long tiempoDeInicio  = System.currentTimeMillis();
		ListaEncadenada<VORating> listaRetornar = new ListaEncadenada<>();
		Iterator<VORating> iterRatings = listaDeRatings.iterator();
		while(iterRatings.hasNext())
		{
			VORating ratingActual = iterRatings.next();
			if(ratingActual.getIdPelicula() == idPelicula)
			{
				listaRetornar.agregarElementoFinal(ratingActual);
			}
		}
		try {
			listaRetornar = (ListaEncadenada<VORating>) Merge.sort(listaRetornar, new ComparadorVORatingTimeStamp());
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("ratingsPeliculaSR", tiempoDeInicio, tiempoDeEjecucion);
		System.out.println(listaRetornar + "\nFINAL");
		return listaRetornar;
	}

	// PARTE B---------------------------------------------------------------

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		long tiempoDeInicio = System.currentTimeMillis();

		ListaEncadenada<VOGeneroUsuario> usuariosActivos = new ListaEncadenada<VOGeneroUsuario>();

		Iterator<VOGeneroUsuario> iterGU = usuariosPorGenero.iterator();

		while (iterGU.hasNext())
		{
			VOGeneroUsuario VO = iterGU.next();
			ILista<VOUsuarioConteo> usuariosActuales = VO.getUsuarios();
			ILista<VOUsuarioConteo> usuariosMasActivos = new ListaEncadenada<VOUsuarioConteo>();
			String generoActual = VO.getGenero();

			try {
				usuariosActuales = Merge.sort(usuariosActuales, new ComparadorVOUsuarioConteo());
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				System.out.println("Error ordenando usuarios de VOGeneroUsuario");
				e.printStackTrace();
			}

			Iterator<VOUsuarioConteo> iterOrdenado = usuariosActuales.iterator();

			int contador = 0;

			while (iterOrdenado.hasNext() && contador < n)
			{
				usuariosMasActivos.agregarElementoFinal(iterOrdenado.next());
				contador++;
			}

			VOGeneroUsuario masActivosDelGenero = new VOGeneroUsuario();

			masActivosDelGenero.setUsuarios(usuariosMasActivos);
			masActivosDelGenero.setGenero(generoActual);

			usuariosActivos.agregarElementoFinal(masActivosDelGenero);
		}
		
		System.out.println(usuariosActivos);
		
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("usuariosActivosSR", tiempoDeInicio, tiempoDeEjecucion);
		return usuariosActivos;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		long tiempoDeInicio = System.currentTimeMillis();

		ILista<VOUsuario> catalogoOrdenado = null;

		try {
			catalogoOrdenado = Merge.sort(listaDeUsuarios, new ComparadorRatingsUsuario());
			listaDeUsuarios = (ListaEncadenada<VOUsuario>) catalogoOrdenado;
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("catalogoUsuariosOrdenadoSR", tiempoDeInicio, tiempoDeEjecucion);
		return catalogoOrdenado;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		long tiempoDeInicio = System.currentTimeMillis();

		Iterator<VOGeneroPelicula> iterGeneros = peliculasPorGenero.iterator();
		ILista<VOGeneroPelicula> mayoresTagsPorGenero = new ListaEncadenada<VOGeneroPelicula>();

		while (iterGeneros.hasNext())
		{

			VOGeneroPelicula actual = iterGeneros.next();
			String generoActual = actual.getGenero();

			VOGeneroPelicula mayorTagGeneroActual = new VOGeneroPelicula();			
			mayorTagGeneroActual.setGenero(generoActual);

			ILista<VOPelicula> pelisOrdenadasTags = new ListaEncadenada<VOPelicula>(); 
			try {
				pelisOrdenadasTags = Merge.sort(actual.getPeliculas(), new ComparadorPeliculasPorTags());
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ILista<VOPelicula> mayoresTags = new ListaEncadenada<VOPelicula>();

			Iterator<VOPelicula> iterOrdenado = pelisOrdenadasTags.iterator();
			int contador = 0;

			while (iterOrdenado.hasNext() && contador < n)
			{
				mayoresTags.agregarElementoFinal(iterOrdenado.next());
				contador++;
			}

			mayorTagGeneroActual.setPeliculas(mayoresTags);

			mayoresTagsPorGenero.agregarElementoFinal(mayorTagGeneroActual);
		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("recomendarTagsGeneroSR", tiempoDeInicio, tiempoDeEjecucion);
		return mayoresTagsPorGenero;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		long tiempoDeInicio = System.currentTimeMillis();

		ILista<VOUsuarioGenero> usuarios = new ListaEncadenada<VOUsuarioGenero>();

		Iterator<VOUsuario> iterUsuarios = listaDeUsuarios.iterator();

		while (iterUsuarios.hasNext())
		{
			VOUsuario usuarioActual = iterUsuarios.next();
			VOUsuarioGenero tagsPorGeneroUsuario = new VOUsuarioGenero();


			Iterator<VOGeneroTag> iterPorGenero = tagsPorGenero.iterator(); 
			ILista<VOGeneroTag> listaDeTagsPorGeneroDeUsuario = new ListaEncadenada<VOGeneroTag>();

			while (iterPorGenero.hasNext())
			{
				VOGeneroTag generoActual = iterPorGenero.next();
				Iterator<VOTag> iterTagsUsuario = usuarioActual.getTags().iterator();


				VOGeneroTag nuevoGenero = new VOGeneroTag();
				nuevoGenero.setGenero(generoActual.getGenero());

				ILista<String> nuevaListaTags = new ListaEncadenada<String>();

				while (iterTagsUsuario.hasNext())
				{
					VOTag tagActualUsuario = iterTagsUsuario.next();
					Iterator<String> iterTagsGeneroActual = generoActual.getTags().iterator();

					while (iterTagsGeneroActual.hasNext())
					{
						String tagActual = iterTagsGeneroActual.next();

						if (tagActualUsuario.getTag().equals(tagActual))
							nuevaListaTags.agregarElementoFinal(tagActual);						
					}

				}


				ILista<String> nuevosTagsOrdenados = new ListaEncadenada<String>();

				try {
					nuevosTagsOrdenados = Merge.sort(nuevaListaTags, new ComparadorTagsAlfabetico());
				} catch (InstantiationException | IllegalAccessException e) {
					e.printStackTrace();}

				nuevoGenero.setTags(nuevosTagsOrdenados);
				listaDeTagsPorGeneroDeUsuario.agregarElementoFinal(nuevoGenero);
				tagsPorGeneroUsuario.setListaGeneroTags(listaDeTagsPorGeneroDeUsuario);	
			}
			tagsPorGeneroUsuario.setIdUsuario(usuarioActual.getIdUsuario());
			usuarios.agregarElementoFinal(tagsPorGeneroUsuario);			
		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("opinionTagsGeneroSR", tiempoDeInicio, tiempoDeEjecucion);
		return usuarios;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(
			String rutaRecomendacion, int n) {
		long tiempoDeInicio = System.currentTimeMillis();

		ILista<VOPeliculaUsuario> usuariosPorPeliRecomendados = new ListaEncadenada<VOPeliculaUsuario>();
		BufferedReader br;
		try
		{
			br = new BufferedReader(new FileReader( new File(RUTA_RECOMENDACION)));
			br.readLine();//Me salto la linea de convenciones


			String linea = br.readLine();
			int contador = 0;

			while (linea != null && contador < n)
			{
				contador++;
				String[] datos = linea.split(",");

				long idPeliculaReciente = Long.parseLong(datos[0]);
				double ratingReciente = Double.parseDouble(datos[1]);
				VOPeliculaUsuario usuariosRecomendadosPeliculaActual = new VOPeliculaUsuario();
				usuariosRecomendadosPeliculaActual.setUsuariosRecomendados(new ListaEncadenada<VOUsuario>());
				usuariosRecomendadosPeliculaActual.setIdPelicula(idPeliculaReciente);

				Iterator<VORating> iterRatings = listaDeRatings.iterator();

				while (iterRatings.hasNext())
				{
					VORating ratingActual = iterRatings.next();

					double diferenciaOpinion = Math.abs(ratingActual.getRating()-ratingReciente); 

					if(ratingActual.getIdPelicula() == idPeliculaReciente &&	// Si el id de ambas pel�culas coincide
							diferenciaOpinion<=0.5)   // Y su diferencia absoluta es menor o igual a 0.5
					{
						VOUsuario match = new VOUsuario();

						match.setDiferenciaOpinion(diferenciaOpinion);
						match.setIdUsuario(ratingActual.getIdUsuario());

						if (usuariosRecomendadosPeliculaActual.getUsuariosRecomendados().darNumeroElementos() == 0)
						{
							ListaEncadenada<VOUsuario> nuevosRecomendados = new ListaEncadenada<VOUsuario>();
							usuariosRecomendadosPeliculaActual.setUsuariosRecomendados(nuevosRecomendados);
						}

						usuariosRecomendadosPeliculaActual.getUsuariosRecomendados().agregarElementoFinal(match);
					}
				}

				ILista<VOUsuario> usuariosOrdenados = new ListaEncadenada<VOUsuario>();
				try {
					usuariosOrdenados = Merge.sort(usuariosRecomendadosPeliculaActual.getUsuariosRecomendados(), new ComparadorDiferenciaRatings());
				} catch (InstantiationException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				usuariosRecomendadosPeliculaActual.setUsuariosRecomendados(usuariosOrdenados);

				usuariosPorPeliRecomendados.agregarElementoFinal(usuariosRecomendadosPeliculaActual);

				linea = br.readLine();
			}

			System.out.println(usuariosPorPeliRecomendados);
			br.close();

		}catch (IOException e)
		{
			System.out.println("Error leyendo / cargando el archivo: " + e);
			e.printStackTrace();
		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("recomendarUsuariosSR", tiempoDeInicio, tiempoDeEjecucion);
		return usuariosPorPeliRecomendados;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		long tiempoDeInicio = System.currentTimeMillis();

		ILista<VOTag> tagsPelicula = new ListaEncadenada<VOTag>();

		ILista<VOTag> listaTagsDesordenada = new ListaEncadenada<VOTag>();
		VOPelicula buscada = buscarPeliculaPorId(idPelicula);		
		ILista<String> tagsBuscada = buscada.getTagsAsociados();
		Iterator<String> iterTagsPeli = tagsBuscada.iterator();

		while (iterTagsPeli.hasNext())
		{
			String tagString = iterTagsPeli.next();
			VOTag tag = buscarTag(tagString);
			listaTagsDesordenada.agregarElementoFinal(tag);
		}

		try {
			tagsPelicula = Merge.sort(listaTagsDesordenada, new ComparadorTagsTimestamp());
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;
		agregarOperacionSR("tagsPeliculaSR", tiempoDeInicio, tiempoDeEjecucion);
		return tagsPelicula;
	}

	private VOTag buscarTag (String tag)
	{
		VOTag buscado = null;

		boolean encontreTag = false;

		Iterator<VOTag> iterTags = listaDeTags.iterator();

		while (!encontreTag && iterTags.hasNext())
		{
			VOTag actual = iterTags.next();

			if (tag.equals(actual.getTag()))
			{
				encontreTag = true;
				buscado = actual;
			}
		}

		return buscado;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) 
	{
		VOOperacion operacionActual = new VOOperacion();
		operacionActual.setTimestampInicio(tinicio);
		operacionActual.setTimestampFin(tfin);
		operacionActual.setOperacion(nomOperacion);
		colaOperaciones.push(operacionActual);
	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		ListaEncadenada<VOOperacion> listaRetornar = new ListaEncadenada<>();
		for(VOOperacion operacion:colaOperaciones)
		{
			listaRetornar.agregarElementoFinal(operacion);
		}
		return listaRetornar;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		colaOperaciones = new Stack<>();
	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		ListaEncadenada<VOOperacion> listaRetornar = new ListaEncadenada<>();	
		int contadorPosicion = 0;
		if(n < colaOperaciones.size())
		{
			while(!colaOperaciones.isEmpty() && contadorPosicion < n)
			{
				listaRetornar.agregarElementoFinal(colaOperaciones.pop());
				contadorPosicion ++;
			}
		}
		else
		{
			while(!colaOperaciones.isEmpty())
			{
				listaRetornar.agregarElementoFinal(colaOperaciones.pop());
			}
		}		
		return listaRetornar;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		int contadorPosicion = 0;
		if(n < colaOperaciones.size())
		{
			while(!colaOperaciones.isEmpty() && contadorPosicion < n)
			{
				colaOperaciones.pop();
				contadorPosicion ++;
			}
		}
		else
		{
			colaOperaciones = new Stack<>();
		}	

	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		VOPelicula peliculaAgregar = new VOPelicula();
		peliculaAgregar.setTitulo(titulo);
		peliculaAgregar.setAgnoPublicacion(agno);
		ListaEncadenada<String> listaGeneros = new ListaEncadenada<>();
		for(String genero:generos)
		{
			listaGeneros.agregarElementoFinal(genero);
		}
		peliculaAgregar.setGenerosAsociados(listaGeneros);

		peliculaAgregar.setIdUsuario(mayorIdDePeliculaEnElSR()+1);

		Iterator<VOAgnoPelicula> iterAgnoPelicula = listaDePeliculas.iterator();
		while(iterAgnoPelicula.hasNext() && iterAgnoPelicula.next().getAgno()<=agno)
		{
			VOAgnoPelicula agnoPelicula = iterAgnoPelicula.next();
			if(agnoPelicula.getAgno() == agno)
			{
				agnoPelicula.getPeliculas().agregarElementoFinal(peliculaAgregar);
			}
		}

		cantidadDePeliculas ++;
		return mayorIdDePeliculaEnElSR()+1;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		VORating ratingAgregar = new VORating();
		ratingAgregar.setIdUsuario(idUsuario);
		ratingAgregar.setIdPelicula(idPelicula);
		ratingAgregar.setRating(rating);
		ratingAgregar.setStamp(System.currentTimeMillis());
		listaDeRatings.agregarElementoFinal(ratingAgregar);
	}

	private long mayorIdDePeliculaEnElSR()
	{
		long idMayor = Long.MIN_VALUE;
		for(VOAgnoPelicula pelicula:listaDePeliculas)
		{
			long idPeliculaActual = pelicula.getAgno();
			if(idMayor<idPeliculaActual)
			{
				idMayor = idPeliculaActual;
			}
		}
		return idMayor;
	}

	public VOPelicula buscarPelicula(long idPelicula)
	{
		Iterator<VOAgnoPelicula> iterAgno = listaDePeliculas.iterator();
		while(iterAgno.hasNext())
		{
			VOAgnoPelicula agnoActual = iterAgno.next();
			ListaEncadenada<VOPelicula> peliculasAgno = (ListaEncadenada<VOPelicula>) agnoActual.getPeliculas();
			Iterator<VOPelicula> iterPeliculas = peliculasAgno.iterator();
			while(iterPeliculas.hasNext())
			{
				VOPelicula peliculaActual = iterPeliculas.next();
				if(peliculaActual.getIdPelicula() == idPelicula)
				{
					return peliculaActual;
				}
			}
		}
		return null;
	}

	public int getCantidadDeTags() {
		return cantidadDeTags;
	}

	public int getCantidadDePeliculas() {
		return cantidadDePeliculas;
	}

	public ListaEncadenada<VOAgnoPelicula> getListaDePeliculas() {
		return listaDePeliculas;
	}

	public ListaEncadenada<String> getListaDeGeneros() {
		return listaDeGeneros;
	}

	public ListaEncadenada<VOUsuario> getListaDeUsuarios() {
		return listaDeUsuarios;
	}

	public static void main(String[] args) {

		long tiempoDeEjecucion = System.currentTimeMillis();

		SistemaRecomendacionPeliculas sistema = new SistemaRecomendacionPeliculas();

		sistema.cargarPeliculasSR(RUTA_PELICULAS);
		sistema.cargarTagsSR(RUTA_TAGS);
		sistema.cargarRatingsSR(RUTA_RATINGS);
		//sistema.cargarTagsSR(RUTA_TAGS);

		System.out.println("Propiedades del SR:"
				+ "\n N�mero de Peliculas = " + sistema.getCantidadDePeliculas()
				+ "\n N�mero de G�neros = " + sistema.getListaDeGeneros().darNumeroElementos()
				+ "\n N�mero de Usuarios = " + sistema.getListaDeUsuarios().darNumeroElementos()
				+ "\n N�mero de Tags = " + sistema.getCantidadDeTags());




		tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeEjecucion;
		System.out.println("Se demor�: " + tiempoDeEjecucion + "ms en iniciar" );

		//PARTE A

		//sistema.peliculasPopularesSR(10);
		//sistema.catalogoPeliculasOrdenadoSR();
		//sistema.recomendarGeneroSR();
		sistema.opinionRatingsGeneroSR();
		//sistema.recomendarPeliculasSR(RUTA_RECOMENDACION, 1);
		//sistema.ratingsPeliculaSR(1);

		//PARTE B

		sistema.usuariosActivosSR(10);
		sistema.catalogoUsuariosOrdenadoSR();
		sistema.recomendarTagsGeneroSR(10);
		sistema.opinionTagsGeneroSR();
		sistema.recomendarUsuariosSR(RUTA_RECOMENDACION, 5);
		System.out.println(sistema.tagsPeliculaSR(1258));
	}

}