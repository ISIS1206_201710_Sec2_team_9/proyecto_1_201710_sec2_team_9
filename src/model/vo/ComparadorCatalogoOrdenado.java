package model.vo;

import java.util.Comparator;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;

public class ComparadorCatalogoOrdenado implements Comparator<VOPelicula>
{
	private ComparadorListaComparadores<VOPelicula> comparadorLista;

	public ComparadorCatalogoOrdenado()
	{
		ComparadorVOPeliculaPorAgno comparadorAgno = new ComparadorVOPeliculaPorAgno();
		ComparadorVOPeliculaPorNombre comparadorNombre = new ComparadorVOPeliculaPorNombre();
		ComparadorVOPeliculaPromedioDeRatingsDescendente comparadorRatings = new ComparadorVOPeliculaPromedioDeRatingsDescendente();
		ILista<Comparator<VOPelicula>> listaComparadores = new ListaEncadenada<>();
		listaComparadores.agregarElementoFinal(comparadorAgno);
		listaComparadores.agregarElementoFinal(comparadorRatings);
		listaComparadores.agregarElementoFinal(comparadorNombre);
		comparadorLista = new ComparadorListaComparadores<>(listaComparadores);
	}

	@Override
	public int compare(VOPelicula pelicula1, VOPelicula pelicula2)
	{
		return comparadorLista.compare(pelicula1, pelicula2);
	}
}
