package model.vo;

import java.util.Comparator;

public class ComparadorDiferenciaRatings implements Comparator<VOUsuario> {

	@Override
	public int compare(VOUsuario arg0, VOUsuario arg1) {
		// TODO Auto-generated method stub
		if (arg0.getDiferenciaOpinion() - arg1.getDiferenciaOpinion() < 0)
			return -1;
		else if (arg0.getDiferenciaOpinion() - arg1.getDiferenciaOpinion() == 0)
			return 0;
		else
			return 1;
	}
	

}
