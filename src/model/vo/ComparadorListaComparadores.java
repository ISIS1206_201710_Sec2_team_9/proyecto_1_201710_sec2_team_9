package model.vo;

import java.util.Comparator;

import model.data_structures.ILista;

public class ComparadorListaComparadores<T> implements Comparator<T>
{
	private ILista<Comparator<T>> comparadores;
	public ComparadorListaComparadores( ILista<Comparator<T>> comparadores)
	{
		this.comparadores = comparadores;
		System.out.println(comparadores.darNumeroElementos());
	}
	@Override
	public int compare(T pelicula1, T pelicula2) 
	{
		for(Comparator<T> comparador: comparadores)
		{
			int resultadoComparacion = comparador.compare(pelicula1, pelicula2);
			if(resultadoComparacion != 0)
			{
				return resultadoComparacion;
			}
		}
		return 0;
	}
	
}
