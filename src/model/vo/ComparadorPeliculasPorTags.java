package model.vo;

import java.util.Comparator;

public class ComparadorPeliculasPorTags implements Comparator<VOPelicula> {

	@Override
	public int compare(VOPelicula arg0, VOPelicula arg1) {
		// TODO Auto-generated method stub
		return arg1.getNumeroTags() - arg0.getNumeroTags();
	}
	

}
