package model.vo;

import java.util.Comparator;

public class ComparadorRatingsUsuario implements Comparator<VOUsuario> {

	@Override
	public int compare(VOUsuario arg0, VOUsuario arg1) {
		// TODO Auto-generated method stub
		
		long timeComparison = arg0.getPrimerTimestamp() - arg1.getPrimerTimestamp();
		
		if( timeComparison == 0)
		{
			int ratingQuantityComparison = arg0.getNumRatings() - arg1.getNumRatings();
			
			if (ratingQuantityComparison == 0)
			{
				if (arg0.getIdUsuario() - arg1.getIdUsuario() > 0)
					return 1;
			}
			else if (ratingQuantityComparison > 0)
				return 1;
		}
		else if (timeComparison >0)
			return 1;
			
		return 0;
	}

}
