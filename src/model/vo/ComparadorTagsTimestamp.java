package model.vo;

import java.util.Comparator;

public class ComparadorTagsTimestamp implements Comparator<VOTag> {

	@Override
	public int compare(VOTag arg0, VOTag arg1) {
		// TODO Auto-generated method stub
		if (arg0.getTimestamp()-arg1.getTimestamp() <0)
			return -1;
		else if (arg0.getTimestamp()-arg1.getTimestamp() == 0)
			return 0;
		else
			return -1;
	}

}
