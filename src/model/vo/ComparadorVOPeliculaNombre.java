package model.vo;

import java.util.Comparator;

public class ComparadorVOPeliculaNombre implements Comparator<VOPelicula>
{
	public ComparadorVOPeliculaNombre()
	{
		
	}
	@Override
	public int compare(VOPelicula pelicula1, VOPelicula pelicula2) 
	{
		return pelicula1.getTitulo().compareTo(pelicula2.getTitulo());		
	}
}
