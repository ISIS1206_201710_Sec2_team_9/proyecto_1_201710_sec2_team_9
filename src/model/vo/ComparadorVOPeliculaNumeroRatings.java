package model.vo;

import java.util.Comparator;

public class ComparadorVOPeliculaNumeroRatings implements Comparator<VOPelicula>
{
	@Override
	public int compare(VOPelicula pelicula1, VOPelicula pelicula2) 
	{
		return -(pelicula1.getNumeroRatings()-pelicula2.getNumeroRatings());
	}
}
