package model.vo;

import java.util.Comparator;

public class ComparadorVOPeliculaPorAgno implements Comparator<VOPelicula>
{

	@Override
	public int compare(VOPelicula pelicula1, VOPelicula pelicula2) 
	{
		return pelicula1.getAgnoPublicacion()-pelicula2.getAgnoPublicacion();
	}

	

}
