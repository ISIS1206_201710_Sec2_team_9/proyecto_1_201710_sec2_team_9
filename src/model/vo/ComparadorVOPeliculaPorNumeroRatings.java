package model.vo;

import java.util.Comparator;

public class ComparadorVOPeliculaPorNumeroRatings implements Comparator<VOPelicula>
{
	public ComparadorVOPeliculaPorNumeroRatings()
	{
		
	}
	@Override
	public int compare(VOPelicula pelicula1, VOPelicula pelicula2) 
	{
		return -(pelicula1.getNumeroRatings() - pelicula2.getNumeroRatings());
		
	}
}
