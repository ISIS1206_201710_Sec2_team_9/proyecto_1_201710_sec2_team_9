package model.vo;

import java.util.Comparator;

public class ComparadorVOPeliculaPromedioDeRatingsDescendente implements Comparator<VOPelicula>
{

	@Override
	public int compare(VOPelicula pelicula1, VOPelicula pelicula2) {
		double resultado = pelicula1.getPromedioRatings()-pelicula2.getPromedioRatings();
		if(resultado < 0)
		{
			return 1;
		}
		else if(resultado > 0)
		{
			return -1;
		}
		return 0;
	}

}
