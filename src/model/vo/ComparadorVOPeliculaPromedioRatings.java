package model.vo;

import java.util.Comparator;

public class ComparadorVOPeliculaPromedioRatings implements Comparator<VOPelicula>
{
	public ComparadorVOPeliculaPromedioRatings()
	{
		
	}
	@Override
	public int compare(VOPelicula pelicula1, VOPelicula pelicula2) 
	{
		double diferencia = pelicula1.getPromedioRatings() - pelicula2.getPromedioRatings();
		if(diferencia > 0)
		{
			return 1;
		}
		else if (diferencia <0)
		{
			return -1;
		}
		return 0;
	}
}
