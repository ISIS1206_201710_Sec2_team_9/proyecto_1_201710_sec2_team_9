package model.vo;

import java.util.Comparator;

public class ComparadorVORatingTimeStamp implements Comparator<VORating>
{

	@Override
	public int compare(VORating rating1, VORating rating2) 
	{
		return (int) ((int) rating1.getStamp() - rating2.getStamp());
	}
	
}
