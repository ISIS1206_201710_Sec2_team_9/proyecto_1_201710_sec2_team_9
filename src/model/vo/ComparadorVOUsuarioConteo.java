package model.vo;

import java.util.Comparator;

public class ComparadorVOUsuarioConteo implements Comparator<VOUsuarioConteo> {

	@Override
	public int compare(VOUsuarioConteo arg0, VOUsuarioConteo arg1) {
		// TODO Auto-generated method stub
		return arg1.getConteo() - arg0.getConteo();
	}

}
