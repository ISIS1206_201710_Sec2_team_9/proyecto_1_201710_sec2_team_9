package model.vo;


import java.util.Iterator;

import model.data_structures.ILista;

public class VOAgnoPelicula {

	private int agno;
	
	private ILista<VOPelicula> peliculas;

	public int getAgno() 
	{
		return agno;
	}

	public void setAgno(int agno)
	{
		this.agno = agno;
	}

	public ILista<VOPelicula> getPeliculas() 
	{
		return peliculas;
	}

	public void setPeliculas(ILista<VOPelicula> peliculas)
	{
		this.peliculas = peliculas;
	}

	public String toString ()
	{
		String respuesta = "";
		
		Iterator<VOPelicula> iter = peliculas.iterator();
		
		while (iter.hasNext())
		{
			VOPelicula peli = iter.next();
			
			respuesta += "|| " + peli.getTitulo() + ", " + peli.getAgnoPublicacion() + " ||";
		}
		
		return respuesta;
	}
	

}
