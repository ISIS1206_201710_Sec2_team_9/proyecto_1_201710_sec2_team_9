package model.vo;

public class VOOperacion {
	
	private String operacion;
	private long timestampInicio;
	private long timestampFin;
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public long getTimestampInicio() {
		return timestampInicio;
	}
	public void setTimestampInicio(long timestampInicio) {
		this.timestampInicio = timestampInicio;
	}
	public long getTimestampFin() {
		return timestampFin;
	}
	public void setTimestampFin(long timestampFin) {
		this.timestampFin = timestampFin;
	}
	public String toString()
	{
		String respuesta = "";
		respuesta += operacion + " Tiempo ejecución: "+ (timestampInicio-timestampFin) + "\n"  ;
		return respuesta;
	}

}
