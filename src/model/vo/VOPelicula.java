package model.vo;




import model.data_structures.ILista;

public class VOPelicula{

	private long idPelicula; 
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;

	//TODO private ListaEncadenada<VORating> listaRatings;

	private ILista<String> tagsAsociados;
	private ILista<String> generosAsociados;	


	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdUsuario(long idUsuario) {
		this.idPelicula = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	public String toString()
	{
		String respuesta = "";
		//"(" + agnoPublicacion + ") " + tagsAsociados + " #Rat: " + 
		//" PromRat:" + promedioRatings + "\n"
		respuesta += titulo +"(" + agnoPublicacion + ") | Rating: "+ promedioRatings+ "  | Tags: "+tagsAsociados + "\n" ;

		return respuesta;
	}


}
