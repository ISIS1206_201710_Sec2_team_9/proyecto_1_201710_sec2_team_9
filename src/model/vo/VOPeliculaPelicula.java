package model.vo;

import model.data_structures.ILista;

public class VOPeliculaPelicula {
	
	private VOPelicula pelicula;
	private ILista<VOPelicula> peliculasRelacionadas;
	
	public VOPelicula getPelicula() {
		return pelicula;
	}
	public void setPelicula(VOPelicula pelicula) {
		this.pelicula = pelicula;
	}
	public ILista<VOPelicula> getPeliculasRelacionadas() {
		return peliculasRelacionadas;
	}
	public void setPeliculasRelacionadas(ILista<VOPelicula> peliculasRelacionadas) {
		this.peliculasRelacionadas = peliculasRelacionadas;
	}
	public String toString()
	{
		return pelicula +": "+peliculasRelacionadas;
	}

}
