package model.vo;

public class VORating {
	
	
	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long stamp;
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public long getStamp() {
		return stamp;
	}
	public void setStamp(long stamp) {
		this.stamp = stamp;
	}
	public String toString()
	{
		String respuesta = "";
		respuesta += "\nEl usuario con id " + idUsuario + " calific� con " + rating ;

		return respuesta;
	}

}
