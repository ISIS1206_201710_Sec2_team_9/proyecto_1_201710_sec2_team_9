package model.vo;

public class VOTag {
	private String tag;
	private long timestamp;
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public String toString ()
	{
		String repsuesta = tag;
		
		return repsuesta;
	}
	
}
