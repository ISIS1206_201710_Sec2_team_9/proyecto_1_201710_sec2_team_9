package model.vo;

public class VOUsuarioConteo {
	private long idUsuario;
	private int conteo;

	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	
	public String toString() {
		String respuesta = "ID: " + idUsuario + " n�mero de ratings: " + conteo;
		
		return respuesta;
	}
	
}
