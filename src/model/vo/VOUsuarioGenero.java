package model.vo;

import model.data_structures.ILista;

public class VOUsuarioGenero {
	
private long idUsuario;
	
	private ILista<VOGeneroTag> listaGeneroTags;
	
	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public ILista<VOGeneroTag> getListaGeneroTags() {
		return listaGeneroTags;
	}

	public void setListaGeneroTags(ILista<VOGeneroTag> listaGeneroTags) {
		this.listaGeneroTags = listaGeneroTags;
	}

	public String toString()
	{
		String respuesta = "IdUsuario: " + idUsuario + "\nLista de tags creados por g�nero: " + listaGeneroTags + "\n--------------\n";
		
		return respuesta;
	}
	
	

}
