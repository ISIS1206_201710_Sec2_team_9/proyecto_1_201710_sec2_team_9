package test;


import java.util.Iterator;

import model.data_structures.ListaEncadenada;
import junit.framework.TestCase;

public class ListaEncadenadaTest extends TestCase{
private ListaEncadenada<String> listaEncadenada;
	
	public void setupEscenarioConTresElementos()
	{
		listaEncadenada = new ListaEncadenada<String>();
		listaEncadenada.agregarElementoFinal("1");
		listaEncadenada.agregarElementoFinal("2");
		listaEncadenada.agregarElementoFinal("3");
	}
	public void setupEscenarioConUnElemento()
	{
		listaEncadenada = new ListaEncadenada<String>();
		listaEncadenada.agregarElementoFinal("1");
	}
	
	public void testCreacionListaEncadenada()
	{
		listaEncadenada = new ListaEncadenada<String>();
		assertEquals("El numero de nodos es incorrecto", 0, listaEncadenada.darNumeroElementos());
		try
		{
			listaEncadenada.darElemento(0);
			fail();
		}
		catch(IndexOutOfBoundsException e)
		{
			
		}
		
	}
	public void testAgregarElemento()
	{
		setupEscenarioConTresElementos();
		assertEquals("El numero de nodos es incorrecto", 3, listaEncadenada.darNumeroElementos());
		assertEquals("1",listaEncadenada.darElemento(0));
		assertEquals("2",listaEncadenada.darElemento(1));
		assertEquals("3",listaEncadenada.darElemento(2));
		
	}
	public void testDarElementoOutOfBounds()
	{
		setupEscenarioConTresElementos();
		try
		{
			listaEncadenada.darElemento(3);
			fail();
		}
		catch(IndexOutOfBoundsException e)
		{
			
		}
	}
	public void testIteradorListaEncadenadaVacia()
	{
		listaEncadenada = new ListaEncadenada<String>();
		Iterator<String> iterador = listaEncadenada.iterator();
		assertFalse(iterador.hasNext());
	}
	public void testIteradorListaEncadenadaConTresElementos()
	{
		setupEscenarioConTresElementos();
		Iterator<String> iterador = listaEncadenada.iterator();
		assertTrue(iterador.hasNext());
		assertEquals("1", iterador.next());
		assertTrue(iterador.hasNext());
		assertEquals("2", iterador.next());
		assertTrue(iterador.hasNext());
		assertEquals("3", iterador.next());
		assertFalse(iterador.hasNext());
	}
	public void testHasNextNoAvanzaIterador()
	{
		setupEscenarioConUnElemento();
		Iterator<String> iterador = listaEncadenada.iterator();
		assertTrue(iterador.hasNext());
		assertTrue(iterador.hasNext());
		iterador.next();
		assertFalse(iterador.hasNext());
		assertFalse(iterador.hasNext());
	}
	public void testRevisarDosIteradoresSonIndependientes()
	{
		setupEscenarioConTresElementos();
		Iterator<String> iterador1 = listaEncadenada.iterator();
		Iterator<String> iterador2 = listaEncadenada.iterator();
		iterador1.next();
		iterador1.next();
		assertEquals("1", iterador2.next());
		assertEquals("3", iterador1.next());
	}
	public void testDarElementoPosicionActualEnListaVacia()
	{
		listaEncadenada = new ListaEncadenada<String>();
		try
		{
			listaEncadenada.darElementoPosicionActual();
			fail();
		}
		catch(IndexOutOfBoundsException e)
		{
			
		}
	}
	public void testDarElementoPosicionActualConTresElementos()
	{
		setupEscenarioConTresElementos();
		System.out.println(listaEncadenada);
		assertEquals("1", listaEncadenada.darElementoPosicionActual());
		assertTrue(listaEncadenada.avanzarSiguientePosicion());
		assertEquals("2", listaEncadenada.darElementoPosicionActual());
		assertTrue(listaEncadenada.avanzarSiguientePosicion());
		assertEquals("3", listaEncadenada.darElementoPosicionActual());
		assertFalse(listaEncadenada.avanzarSiguientePosicion());
		assertTrue(listaEncadenada.retrocederPosicionAnterior());
		assertEquals("2", listaEncadenada.darElementoPosicionActual());
		assertTrue(listaEncadenada.retrocederPosicionAnterior());
		assertEquals("1", listaEncadenada.darElementoPosicionActual());
		assertFalse(listaEncadenada.retrocederPosicionAnterior());		
	}
	
	public void testDarElementoPosicionActualDespuesDeAdicionarElemento()
	{
		setupEscenarioConTresElementos();
		assertTrue(listaEncadenada.avanzarSiguientePosicion());
		assertTrue(listaEncadenada.avanzarSiguientePosicion());
		assertFalse(listaEncadenada.avanzarSiguientePosicion());
		listaEncadenada.agregarElementoFinal("4");
		assertTrue(listaEncadenada.avanzarSiguientePosicion());
		assertEquals("4", listaEncadenada.darElementoPosicionActual());
	}
}
