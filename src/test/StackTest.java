package test;
import static org.junit.Assert.*;
import model.data_structures.Stack;

import org.junit.Test;

public class StackTest {

	/**
	 * Lista a evaluar
	 */
	private Stack<String> pila;
	
	/**
	 * Crea una lista y le asigna pocos valores arbitrarios
	 */
	private void ambiente1()
	{
		pila = new Stack<String>();
		
		pila.push("Holigrama");
		pila.push("Chaograma");
		pila.push("Writable");
		pila.push("Neon");
		pila.push("Kepler");
	}
	
	/**
	 * Crea una lista y le asigna una cantidad espec�fica de elementos dados por par�metro<br>
	 * Se usa para probar con grandes cantidades de elementos
	 * @param numElementos cantidad de elementos a agregar
	 */
	private void ambiente2(int numElementos)
	{
		pila = new Stack<String>();
		
		//Se agrega desde elem: 0 hasta elem: n, osea que
		//El tama�o de la lista es numElementos
		//El �ndice m�ximo que funciona darELemento es numElementos - 1
		for (int i = 0; i < numElementos; i++)
		{
			pila.push("elem: " +i);
		}
	}

	//hacer dequeue un numero n de veces
	private void popVariasVeces(int n)
	{
		for(int i = 0; i < n; i++)
			System.out.println("Saqu� a: " + pila.pop());
	}
	
	@Test
	public void test() {
		
		//Pocos valores arbitrarios
		ambiente1();
		
		assertEquals("String incorecto", "Kepler", pila.pop());
		
		System.out.println(pila.pop());
		System.out.println(pila.pop());
		
		assertEquals("String incorecto", "Chaograma", pila.pop());
		
		assertEquals("Cantidad err�nea", 1, pila.size());
	
		pila.pop();
		
		assertEquals("Cantidad err�nea", 0, pila.size());
		
		assertTrue(pila.isEmpty());
		
		//Cantidad un poco mayor de valores
		ambiente2(100);
		
		//hago 9 veces, porque quiero llegar al 91
		popVariasVeces(9);
		
		assertEquals("String incorecto", "elem: 90", pila.pop());
		
		popVariasVeces(69);
		
		assertEquals("String incorecto", "elem: 20", pila.pop());
		
		popVariasVeces(19);
		
		assertEquals("String incorecto", "elem: 0", pila.pop());
		
		//no hay nada porque la elemento 99 (el ultimo) le hice dequeue()
		assertEquals("Cantidad err�nea", 0, pila.size());
		
		//Esto no deber�a poder hacerse
		try
		{
			pila.pop();
			fail();
		}
		catch (Exception e)
		{
			System.out.println("bien!");
		}
		
		assertTrue(pila.isEmpty());
		
	}
	
}